namespace Malquarium.Sltp.Tests {

    using System;
    using System.Linq;
    using System.IO;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using MiniTest;
    using Malquarium.Sltp;
    
    public class UnitTests {

        public static void Main(string[] args) {
            var tests = new MiniTestSuite();

            tests.Add(new TestCase("Parsing correct message (1)", Test_Parse_HappyPath1));
            tests.Add(new TestCase("Parsing correct message (2)", Test_Parse_HappyPath2));
            tests.Add(new TestCase("Fail on invalid PHEAD", Test_Invalid_PHEAD));
            tests.Add(new TestCase("Fail on invalid DHEAD", Test_Invalid_DHEAD));
            tests.Add(new TestCase("Fail on DATA longer than specified", Test_DATA_too_long));
            tests.Add(new TestCase("Fail on DATA shorter than specified", Test_DATA_too_short));
            tests.Add(new TestCase("Fail on size does not match dimensions", Test_Size_Does_Not_Match_Pixels));
            tests.Add(new TestCase("Flip RGBA data along Y axis", Test_FlipRgbaData));
            tests.Add(new TestCase("Cloning via backup serialization works", Test_CloneViaBackupSerialization));
            tests.Add(new TestCase("FishData with invalid dimensions throws exception.", Test_ExceptionOnInvalidDimensions));

            tests.Run();
        }

        private static async Task Test_Parse_HappyPath1() {
            var message = new byte[] {
                // PHEAD
                1,0,0,0,
                8,0,0,0, // dlen: 8byte == 2px
                // DHEAD
                2,0,0,0, // type: 2
                2,0,1,0, // size: 2*1 px
                // DATA
                1,2,3,4,
                5,6,7,8
            };

            var sut = new SltpV1((_, s)=> { });

            using (var stream = new MemoryStream(message)) {
                var data = await sut.TryReadFishFromStreamAsync(
                    stream,
                    CancellationToken.None);
                MiniAssert.NotEqual(data, null);
                MiniAssert.Equal(data.Type, FishType.Fish2);
                MiniAssert.Equal(data.Height, 1, "(height)");
                MiniAssert.Equal(data.Width, 2, "(width)");
                MiniAssert.Equal(data.RgbaData.Count, 8, "(data len)");
            }
        }

        private static async Task Test_Parse_HappyPath2() {
            var message = new byte[] {
                // PHEAD
                1,0,0,0,
                24,0,0,0, // dlen: 24byte == 6px
                // DHEAD
                4,0,0,0, // type: 2
                3,0,2,0, // size: 3*2 px
                // DATA
                1,2,3,4,
                5,6,7,8,
                1,2,3,4,
                5,6,7,8,
                1,2,3,4,
                5,6,7,8
             };

            var sut = new SltpV1((_, s)=> { });

            using (var stream = new MemoryStream(message)) {
                var data = await sut.TryReadFishFromStreamAsync(
                    stream,
                    CancellationToken.None);
                MiniAssert.NotEqual(data, null, "because success");
                MiniAssert.Equal(data.Type, FishType.Fish4);
                MiniAssert.Equal(data.Height, 2, "(height)");
                MiniAssert.Equal(data.Width, 3, "(width)");
                MiniAssert.Equal(data.RgbaData.Count, 24, "(data length)");
            }
        }

        private async static Task Test_Invalid_PHEAD() {
            var message = new byte[] {
                // PHEAD
                1,1,0,0,
                8,0,0,0,
                // DHEAD
                2,0,0,0, // type: 2
                2,0,1,0, // size: 2*1 px
                // DATA
                1,2,3,4,
                5,6,7,8
            };

            var log = "";
            var sut = new SltpV1((_, s) => log = s);

            using (var stream = new MemoryStream(message)) {
                var data = await sut.TryReadFishFromStreamAsync(
                    stream,
                    CancellationToken.None);
                MiniAssert.Equal(data, null);
                MiniAssert.Contains(log, "PHEAD", "because byte 2 should be 0");
            }
        }

        private async static Task Test_Invalid_DHEAD() {
            var message = new byte[] {
                // PHEAD
                1,0,0,0,
                8,0,0,0,
                // DHEAD
                2,0,8,0, // the 8 should be 0.
                2,0,1,0,
                // DATA
                1,2,3,4,
                5,6,7,8
            };

            var log = "";
            var sut = new SltpV1((_, s) => log = s);

            using (var stream = new MemoryStream(message)) {
                var data = await sut.TryReadFishFromStreamAsync(
                    stream,
                    CancellationToken.None);
                MiniAssert.Equal(data, null);
                MiniAssert.Contains(log, "DHEAD", "because a byte should be 0");
            }
        }

        private async static Task Test_DATA_too_long() {
            var message = new byte[] {
                // PHEAD
                1,0,0,0,
                4,0,0,0,
                // DHEAD
                2,0,0,0,
                1,0,1,0,
                // DATA
                1,2,3,4,
                5,6,7,8
            };

            var log = "";
            var sut = new SltpV1((_, s) => log = s);

            using (var stream = new MemoryStream(message)) {
                var data = await sut.TryReadFishFromStreamAsync(
                    stream,
                    CancellationToken.None);
                MiniAssert.Equal(data, null);
                MiniAssert.Contains(
                    log,
                    "DATA",
                    "because the data block is too long");
            }
        }

        private async static Task Test_DATA_too_short() {
            var message = new byte[] {
                // PHEAD
                1,0,0,0,
                12,0,0,0,
                // DHEAD
                2,0,0,0,
                3,0,1,0,
                // DATA
                1,2,3,4,
                5,6,7,8
            };

            var log = "";
            var sut = new SltpV1((_, s) => log = s);

            using (var stream = new MemoryStream(message)) {
                var data = await sut.TryReadFishFromStreamAsync(
                    stream,
                    CancellationToken.None);
                MiniAssert.Equal(data, null);
                MiniAssert.Contains(log, "DATA", "because the data block is too short.");
            }
        }

        private async static Task Test_Size_Does_Not_Match_Pixels() {
            var message = new byte[] {
                // PHEAD
                1,0,0,0,
                8,0,0,0,
                // DHEAD
                2,0,0,0,
                2,0,2,0,
                // DATA
                1,2,3,4,
                5,6,7,8
            };

            var log = "";
            var sut = new SltpV1((_, s) => log = s);

            using (var stream = new MemoryStream(message)) {
                var data = await sut.TryReadFishFromStreamAsync(
                    stream,
                    CancellationToken.None);
                MiniAssert.Equal(data, null);
                MiniAssert.Contains(log, "dimensions", "because 8 byte implies 8/4 = 2 pixels");
            }
        }

        private static void Test_FlipRgbaData() {

            var testData = new byte[] {
                0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
                0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18,
                0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28,
                0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38,
            };

            var expected = new byte[] {
                0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38,
                0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28,
                0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18,
                0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
            };

            var testFish = new FishTextureData(
                FishType.Fish1,
                2,
                4,
                testData);

            MiniAssert.Equivalent(
                testFish.GetRgbaDataFlippedY(),
                expected,
                "because all rows should be re-ordered");
        }

        private static void Test_CloneViaBackupSerialization() {

            var testData = new byte[] {
                0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
                0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18,
                0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28,
                0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38,
            };

            var orig = new FishTextureData(
                FishType.Fish1,
                2,
                4,
                testData);

            FishTextureData copy;

            using(var stream = new MemoryStream()) {
                BackupSerialization.WriteToStream(stream, orig);
                stream.Seek(0, SeekOrigin.Begin);
                copy = BackupSerialization.ReadFromStream(stream);
            }

            MiniAssert.Equal(copy.Type, orig.Type);
            MiniAssert.Equal(copy.Width, orig.Width);
            MiniAssert.Equal(copy.Height, orig.Height);

            MiniAssert.Equivalent(
                copy.RgbaData.ToArray(),
                orig.RgbaData.ToArray());
        }

        private static void Test_ExceptionOnInvalidDimensions() {
            var testData = new byte[] {
                0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
                0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18,
                0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28,
                0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38,
            };

            try {
                var orig = new FishTextureData(
                    FishType.Fish1,
                    2,
                    8, // this is too much, should be 4. Or the width 1.
                    testData);
            } catch (ArgumentException) {
                // success!
                return;
            }

            MiniAssert.Fail("should throw exception on invalid dimensions.");
        }
    }
}

