
namespace Malquarium.Sltp {

    using System;
    using System.Diagnostics;
    using System.IO;

    /// <summary>
    ///     Configuration settings for the <see cref="TextureServer" /> class.
    /// </summary>
    public class TextureServerSettings {

        /// <summary>
        ///     Initializes a new instance of the
        ///     <see cref="TextureServerSettings"/>.
        /// </summary>
        public TextureServerSettings() {

            this.BackupFolderPath =
                Path.Combine(
                    Environment.GetFolderPath(
                        Environment.SpecialFolder.LocalApplicationData),
                    "malquarium",
                    "bak");

            this.LoggerAction = (level, message) => {
                if (level <= TraceLevel.Info) {
                    Console.WriteLine(message);
                }
            };
        }

        /// <summary>
        ///     Gets or sets the port that the server shall listen on.
        /// </summary>
        /// <remarks>
        ///     The default value is 4242.
        /// </remarks>
        public int TcpPort { get; set; } = 4242;

        /// <summary>
        ///     Gets or sets the action that shall be used to write log messages.
        /// </summary>
        /// <remarks>
        ///     The default action writes all messages with level higher than
        ///     TraceLevel.Info to the Console.
        /// </remarks>
        // set by ctor
        public Action<TraceLevel, string> LoggerAction { get; set; }

        /// <summary>
        ///     Gets or sets the value that controls if the
        ///     <see cref="TextureServer"/> writes messages to a backup and
        ///     reads them to the receive queue on restart.
        /// </summary>
        /// <remarks>
        ///     The default value is <see langword="false"/>.
        /// </remarks>
        public bool UseFileBackup { get; set; } = false;

        /// <summary>
        ///     Gets or sets the folder path that backup files will be written
        ///     to (and searched for).
        /// </summary>
        /// <remarks>
        ///     The default value is see
        ///     "<see cref="Environment.SpecialFolder.LocalApplicationData"/>
        ///     /malquarium/bak"
        /// </remarks>
        public string BackupFolderPath { get; set; } // set by ctor

        /// <summary>
        ///     Gets or sets the value controlling the order in which the
        ///     Texture server will emit the received data.
        /// </summary>
        /// <remarks>
        ///     The default value is <see cref="TextureBufferType.Queue"/>.
        /// </remarks>
        public TextureBufferType BufferType { get; set; } =
            TextureBufferType.Queue;

    }
}
