
namespace Malquarium.Sltp {

    /// <summary>
    ///     Specifies in which order received textures shall
    ///     be popped by the server.
    /// </summary>
    public enum TextureBufferType {

        /// <summary>
        ///     Definens the server's behaviour as first-in-first-out
        ///     (FIFO).
        /// </summary>
        Queue,

        /// <summary>
        ///     Definens the server's behaviour as first-in-last-out
        ///     (FILO).
        /// </summary>
        Stack
    }
}
