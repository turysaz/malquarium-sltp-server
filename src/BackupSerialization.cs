
namespace Malquarium.Sltp {
    using System.IO;
    using System.Linq;
    using System.Text;

    internal static class BackupSerialization {

        public static void WriteToStream(
                Stream stream,
                FishTextureData data) {
            using (var writer = new BinaryWriter(
                    stream,
                    Encoding.UTF8,
                    leaveOpen: true)) {
                writer.Write((int)data.Type);
                writer.Write(data.Width);
                writer.Write(data.Height);
                writer.Write(data.RgbaData.ToArray());
            }
        }

        public static FishTextureData ReadFromStream(Stream stream) {
            using (var reader = new BinaryReader(
                    stream,
                    Encoding.UTF8,
                    leaveOpen: true)) {
                var fishType = reader.ReadInt32();
                var width = reader.ReadInt32();
                var height = reader.ReadInt32();
                var rgbaData = reader.ReadBytes(width * height * 4);

                return new FishTextureData(
                    (FishType)fishType,
                    width,
                    height,
                    rgbaData);
            }
        }
    }
}
