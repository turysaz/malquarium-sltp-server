using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("LibTextureServer")]
[assembly: AssemblyDescription("SLTP implementation")]
[assembly: AssemblyProduct("LibTextureServer")]
[assembly: AssemblyCopyright("Copyright © Turysaz 2022")]

[assembly: AssemblyVersion("0.1.2.0")]
[assembly: AssemblyFileVersion("0.1.2.0")]

[assembly: InternalsVisibleTo("UnitTests")]

