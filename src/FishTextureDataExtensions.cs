
namespace Malquarium.Sltp {
    using System;
    using System.Linq;
    using System.Collections.ObjectModel;

    /// <summary>
    ///     Defines extension methods for the
    ///     <see cref="FishTextureData" /> class.
    /// </summary>
    public static class FishTextureDataExtensions {

        /// <summary>
        ///     Gets the RGBA data of the <paramref name="data"/> with
        ///     flipped Y axis.
        /// </summary>
        /// <remarks>
        ///     Use this method if you need the texture with the origin in
        ///     the bottom left instead of top-left.
        /// </remarks>
        public static byte[] GetRgbaDataFlippedY(
                this FishTextureData data) {
            const int channels = 4;
            int rowLength = data.Width * channels;
            var flipped = Enumerable.Empty<byte>();
            var orig = data.RgbaData;

            for (var row = 1; row <= data.Height; row++) {
                flipped = flipped.Concat(orig
                    .Skip((data.Height - row) * rowLength)
                    .Take(rowLength));
            }

            return flipped.ToArray();
        }
    }
}
