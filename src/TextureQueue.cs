
namespace Malquarium.Sltp {

    using System.Collections.Concurrent;

    internal class TextureQueue
        : ConcurrentQueue<FishTextureData>, ITextureBuffer
    {
        /// <inheritdoc/>
        public void Push(FishTextureData data) => this.Enqueue(data);

        /// <inheritdoc/>
        public bool TryPopNext(out FishTextureData data) =>
            this.TryDequeue(out data);
    }
}

